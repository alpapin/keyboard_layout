# Stalhrim keyboard layout

## What is Stalhrim keyboard layout?

Stalhrim keyboard layout is a variant of the Microsoft french (France) keyboard layout.
The purpose of this layout is adding some missing characters commonly used in french and several mathematical useful characters.

## Description

The character in the bottom left corner is the default key.
The character in the top left corner is the key with `Shift` modifier.
The character in the bottom right corner is the key with `AltGr` modifier.
The character in the top right corner is the key with `Shift` and `AltGr` modifiers.

The default behavior of the Microsoft french (France) keyboard layout is kept except for:
* The grave accent character `` ` `` on scancode `08` (with `è`) which is no more a dead key. This dead key is replaced with `^` and `¨` on scancode `1a`.
* The currency sign `¤` on scancode `1b` (with `$`) has been removed.

Two new dead keys are on the scancode `29` (with `²`). The first is for superscripts and is rendered with the character `ˆ`. The second is for subscripts and is rendered with the character `ˬ`. The possible combinations are:

| Superscripts    | Subscripts      |
| --------------- | --------------- |
| `ˆ` + `1` → `¹` | `ˬ` + `1` → `₁` |
| `ˆ` + `2` → `²` | `ˬ` + `2` → `₂` |
| `ˆ` + `3` → `³` | `ˬ` + `3` → `₃` |
| `ˆ` + `4` → `⁴` | `ˬ` + `4` → `₄` |
| `ˆ` + `5` → `⁵` | `ˬ` + `5` → `₅` |
| `ˆ` + `6` → `⁶` | `ˬ` + `6` → `₆` |
| `ˆ` + `7` → `⁷` | `ˬ` + `7` → `₇` |
| `ˆ` + `8` → `⁸` | `ˬ` + `8` → `₈` |
| `ˆ` + `9` → `⁹` | `ˬ` + `9` → `₉` |
| `ˆ` + `0` → `⁰` | `ˬ` + `0` → `₀` |
| `ˆ` + `+` → `⁺` | `ˬ` + `+` → `₊` |
| `ˆ` + `-` → `⁻` | `ˬ` + `-` → `₋` |
| `ˆ` + `=` → `⁼` | `ˬ` + `=` → `₌` |
| `ˆ` + `(` → `⁽` | `ˬ` + `(` → `₍` |
| `ˆ` + `)` → `⁾` | `ˬ` + `)` → `₎` |
| `ˆ` + `n` → `ⁿ` | `ˬ` + `k` → `ₖ` |
|                 | `ˬ` + `l` → `ₗ` |
|                 | `ˬ` + `m` → `ₘ` |
|                 | `ˬ` + `n` → `ₙ` |

### Stalhrim keyboard layout

![A keyboard](image/layout.png)

### Stalhrim keyboard layout with Caps Lock pressed

![A keyboard](image/layout_capslocked.png)

## Installation

:warning: It requires administrator rights.

### From setup file

1. Go to [Release page](https://gitlab.com/Papin/keyboard_layout/-/releases) and download last release archive.
2. Unzip the archive.
3. Execute `setup.exe`.

### From source

1. Download `stalhrim_layout.klc`.
2. Load it in Microsoft Keyboard Layout Creator.
3. Press `Build DLL and Setup Package` under the `Project` menu list.
4. Execute `setup.exe` in the build directory.

***

Author: Alexandre Papin

Project made with [Microsoft Keyboard Layout Creator 1.4](https://www.microsoft.com/en-us/download/details.aspx?id=22339).
