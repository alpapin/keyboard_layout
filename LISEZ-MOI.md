# Stalhrim keyboard layout

## Qu'est-ce que Stalhrim keyboard layout ?

Stalhrim keyboard layout est une variante de la disposition clavier  française (France) de Microsoft.
Le but de cette disposition est d'ajouter certains caractères manquants couramment utilisés dans la langue française ainsi que quelques symboles mathématiques utiles.

# Description

Le caractère dans le coin en bas à gauche est le symbole par défaut.
Le caractère dans le coin en haut à gauche est le symbole lors du maintien de la touche `Shift`.
Le caractère dans le coin en bas à droite est le symbole lors du maintien de la touche `ALtGr`.
Le caractère dans le coin en haut à droite est le symbole lors du maintien simultané des touches `Shift` et `AltGr`.

Les actions par défaut de la disposition française (France) de Microsoft sont conservées excepté pour les touches suivantes :
* L'accent grave `` ` `` sur la touche `08` (avec `è`) n'est plus une touche morte. Cette touche morte est replacée avec `^` et `¨` sur la touche `1a`.
* Le symbole monétaire `¤` sur la touche `1b` (avec `$`) a été supprimé.

Deux nouvelles touches mortes sont sur la touche `29` (avec `²`). La première est pour les exposants et apparaît avec le symbole `ˆ`. La deuxième est pour les indices et apparaît avec le symbole `ˬ`. Les combinaisons possibles sont :

| Exposants       | Indices         |
| --------------- | --------------- |
| `ˆ` + `1` → `¹` | `ˬ` + `1` → `₁` |
| `ˆ` + `2` → `²` | `ˬ` + `2` → `₂` |
| `ˆ` + `3` → `³` | `ˬ` + `3` → `₃` |
| `ˆ` + `4` → `⁴` | `ˬ` + `4` → `₄` |
| `ˆ` + `5` → `⁵` | `ˬ` + `5` → `₅` |
| `ˆ` + `6` → `⁶` | `ˬ` + `6` → `₆` |
| `ˆ` + `7` → `⁷` | `ˬ` + `7` → `₇` |
| `ˆ` + `8` → `⁸` | `ˬ` + `8` → `₈` |
| `ˆ` + `9` → `⁹` | `ˬ` + `9` → `₉` |
| `ˆ` + `0` → `⁰` | `ˬ` + `0` → `₀` |
| `ˆ` + `+` → `⁺` | `ˬ` + `+` → `₊` |
| `ˆ` + `-` → `⁻` | `ˬ` + `-` → `₋` |
| `ˆ` + `=` → `⁼` | `ˬ` + `=` → `₌` |
| `ˆ` + `(` → `⁽` | `ˬ` + `(` → `₍` |
| `ˆ` + `)` → `⁾` | `ˬ` + `)` → `₎` |
| `ˆ` + `n` → `ⁿ` | `ˬ` + `k` → `ₖ` |
|                 | `ˬ` + `l` → `ₗ` |
|                 | `ˬ` + `m` → `ₘ` |
|                 | `ˬ` + `n` → `ₙ` |

### Stalhrim keyboard layout

![Un clavier](image/layout.png)

### Stalhrim keyboard layout avec Caps Lock pressée

![Un clavier](image/layout_capslocked.png)

## Installation

:warning: L'installation requiert des privilèges d'administrateur.

### Depuis l'exécutable d'installation

1. Allez sur la page [Release](https://gitlab.com/Papin/keyboard_layout/-/releases) et téléchargez l'archive correspondant à la dernière version de la disposition.
2. Désarchivez-la.
3. Exécutez `setup.exe`.

### Depuis les sources

1. Téléchargez `stalhrim_layout.klc`.
2. Chargez ce fichier dans Microsoft Keyboard Layout Creator.
3. Appuyez sur `Build DLL and Setup Package` dans le menu déroulant `Project`.
4. Exécutez `setup.exe` situé dans le dossier généré par l'étape précédente.

***

Auteur : Alexandre Papin

Projet réalisé avec [Microsoft Keyboard Layout Creator 1.4](https://www.microsoft.com/en-us/download/details.aspx?id=22339).
